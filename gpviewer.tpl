<div  class="photobackground ">
<!-- /style css3 -->
<div id="fullsc_css3" style="background-color:#{$color}; width: 100%; height: 100%;">

	<span style="right: 10; top: 10; position: inherit;"><a onClick="goFullScreen_css3();" href="#"><img src="{$url_md}/img/log_out.png" width="32px" title="Close" border="0" style=" vertical-align: middle; "/></a></span>
	<div id="slide">

	<div class="slider">
  
  {foreach item=img_show from=$img key=index name=count}
  	<input type="radio" name="slide_switch" {if $smarty.foreach.count.index eq '0'} checked="checked"{/if}  id="id{$smarty.foreach.count.index}"/>
		<label for="id{$smarty.foreach.count.index}">
			<img class="img_thb" src="{$img_show}-medium.jpg"/>
		</label>
		<img class="img_v" src="{$img_show}-thickbox.jpg"/>
   {/foreach}
	</div>

<script src="{$url_md}/js/prefixfree.js" type="text/javascript"></script>

		</div>
		
</div>
<!-- /style css3 -->
<!-- style appple -->
<div id="fullsc_app" style="background-color:#ffffff; width: 100%; height: 100%;">


	<span style="right: 10; top: 10; position: inherit;"><a onClick="goFullScreen_css3();" href="#"><img src="{$url_md}/img/log_out.png" width="32px" title="Close" border="0" style=" vertical-align: middle; "/></a></span>
    <div id="main_app">
      <div id="gallery_app">	
        <div id="slides_app"  style="background-color:#{$color};">
        	{foreach item=img_show from=$img key=index name=count}
          	<div class="slide_app"><img src="{$img_show}-thickbox.jpg" alt="side" /></div>
          {/foreach}
        </div>
     
        <div id="menu_app">    
        <ul>
            <li class="fbar_app">&nbsp;</li>
            {foreach item=img_show from=$img key=index name=count}
             <li class="menuItem_app"><a href=""><img src="{$img_show}-medium.jpg" width="32" height="32" alt="thumbnail" /></a></li>
            {/foreach}
        </ul>    
        </div>
        
      </div>
        
    </div>
		
</div>
<!-- /style apple -->

</div>	
	
<script>

 function show(id) {
 
    document.getElementById(id).style.visibility = "visible";
  }
  function hide(id) {
  
    document.getElementById(id).style.visibility = "hidden";
  }
	//fullscreen with css3
 function goFullScreen_css3(){
	var picElement = document.getElementById("fullsc_css3");
	if (!document.mozFullScreen && !document.webkitFullScreen) {
	  if (picElement.mozRequestFullScreen) {
		picElement.mozRequestFullScreen();
	  } else {
		picElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		
	  }
	} else {
	  if (document.mozCancelFullScreen) {
		
		document.mozCancelFullScreen();
	  } else {
		
		document.webkitCancelFullScreen();
	  }
	}
 }
 //fullscreen style_app
 function goFullScreen_app(){
	var picElement = document.getElementById("fullsc_app");
	if (!document.mozFullScreen && !document.webkitFullScreen) {
	  if (picElement.mozRequestFullScreen) {
		picElement.mozRequestFullScreen();
	  } else {
		picElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		
	  }
	} else {
	  if (document.mozCancelFullScreen) {
		
		document.mozCancelFullScreen();
	  } else {
		
		document.webkitCancelFullScreen();
	  }
	}
 }
 </script>