<?php
/* Module name: gpviewer
* by GeekPolis.com 2011
*
* This slideshow module is allows easy set up
* Provide multi style profuct viewer
*
* @author prestashopgeek.com <webmaster@prestashopgeek.com>
* @copyright 2012 prestashopgeek.com
* @file Gpviewer.php
* @version Release: 1.4.x - 2.2
*/
	if (!defined('_PS_VERSION_'))
	exit;
	
class Gpviewer extends Module{
		function __construct(){
			$this->name = "gpviewer";
			$this->tab = "Front Office Feature";
			$this->version = "1.0";
			$this->author = "Geekpolis";
			
			parent::__construct();
				
			$this->page = basename(__FILE__,'.php');
			$this->displayName = $this->l('gpviewer');
			$this->description = $this->l('Provide multi style profuct viewer');
			$this->confirmUninstall = $this->l('Are you sure to uninstall Gpviewer images?');
		}
		
		public function install(){
			if(!parent::install() or !$this->registerHook('header') || !$this->registerHook('extraRight')
			){
				return false;
			}
			else{				
			
			Configuration::updateValue('option_style', 'css3');
			Configuration::updateValue('style_zoom',1);
			Configuration::updateValue('bg_color', 'ffffff');
			return true;
			}
		}
		
		public function uninstall(){
			if(!parent::uninstall()){return false;}
			else{
				mysql_query("DROP TABLE "._DB_PREFIX_."geekcf");
			return true;
			}
		}
		
		public function setOption($option_style,$style_zoom,$bg_color)
					{
							Configuration::updateValue('option_style', $option_style);
							Configuration::updateValue('style_zoom', $style_zoom);
							Configuration::updateValue('bg_color', $bg_color);
					}
	
	public function getContent(){
			$this->_html = '';
			if (Tools::isSubmit('submit_conf')) { 
				$this->_html .= $this->displayConfirmation($this->l('The configuration has been saved '));
				$this->setOption(Tools::getValue('option_style'),Tools::getValue('style_zoom'),Tools::getValue('bg_color'));
			}
			
			$this->_displayForm();
			
			return $this->_html;
		}
		public function _displayForm(){
			
			//echo Configuration::get('option_style').'---------';
			$this->_html .= '<html><head>
			<link href="'._MODULE_DIR_.$this->name.'/css/config.css" rel="stylesheet" type="text/css">
			<script type="text/javascript" src="'._MODULE_DIR_.$this->name.'/js/jscolor.js"></script>
			</head><body>
			<center><div id="zoomgk">';
			//fullscreen
			$this->_html .= '<form action="" method="post" name="config"><table id="gp-full" class="altrowstable" border="1">
			<tr><td style="background-color:#000;" colspan="2" align="center"><a href="http://prestashopgeek.com" target="_blank"><img width="100px" src="'._MODULE_DIR_.$this->name.'/img/logo.png" title="Get more modules for your shop!" border="0"/></a><span class="textcf">Gpviewer Fullcreen</span></td></tr>';

				//enable zoom images
			$this->_html .= '<tr ><td><img src="'._MODULE_DIR_.$this->name.'/img/zoom.png"/>Enable Zoom</td>
			<td><div class="switch switch-blue"><input class="switch-input" type="radio" id="zoomy" name="style_zoom" value="1"'; if(Configuration::get('style_zoom') == 1) $this->_html .= 'checked';
			$this->_html .= '/><label for="zoomy" class="switch-label switch-label-off">Yes</label>
			<input class="switch-input" id="zoomn" type="radio" name="style_zoom" value="0"';	
			if(Configuration::get('style_zoom') == 0) $this->_html .= 'checked';
			$this->_html .= '/> <label for="zoomn" class="switch-label switch-label-on">No</label> <span class="switch-selection"></span></div></td></tr>';

			//style
			$this->_html .= '<tr bgcolor="#BFE7E5"><td><img src="'._MODULE_DIR_.$this->name.'/img/effects.png"/> Select Styles</td>
			<td><div class="select_cf"><select onchange="change_cf()" name="option_style"/>
			<option value = "css3" '; 
			if(Configuration::get('option_style') == "css3") $this->_html.=' selected="selected" ';
			$this->_html .= '>Style Css3</option>
			<option value= "app"';
			if(Configuration::get('option_style') == "app") $this->_html.=' selected="selected" ';
			$this->_html .= '>Style Apple</option>
			<option value= "flash"';
			if(Configuration::get('option_style') == "flash") $this->_html.=' selected="selected" ';
			$this->_html .= '>Style Flash</option>  
    </select>
		</div>
		</td></tr>';
		
				//color
			$this->_html .= '<tr><td><img src="'._MODULE_DIR_.$this->name.'/img/color.png"/> Stage Color </td><td colspan="2"><input name="bg_color" class="color" value="'.Configuration::get('bg_color').'" ></td></tr>';
			
			$this->_html .= '<tr><td colspan="3" align="center"><div class="buttons">
			<button type="submit" class="positive" name="submit_conf">
					<img src="'._MODULE_DIR_.$this->name.'/img/apply.png" alt=""/>
					Save
			</button>
			</td></tr></table></form>';			
			$this->_html .='</div>
			</center>
			</body></html>';			
		}
		
			
		public function hookHeader ($params){
			global $smarty;
			
			//unset css 'fancybox'
			global $css_files;
			unset ($css_files[_PS_CSS_DIR_ . 'jquery.fancybox-1.3.4.css']);
			
			$js = "";
			$css = "";
						
			
			//js slide css3
			$js = '<script type = "text/javascript" src = "'._MODULE_DIR_.$this->name.'/js/prefixfree.js"></script>';
			//js slide app
			$js .= '<script type = "text/javascript" src = "'._MODULE_DIR_.$this->name.'/js/script_app.js"></script>';
			//css slide app
			$css .= '<link type = "text/css" href = "'._MODULE_DIR_.$this->name.'/css/slide_app.css" rel = "stylesheet"/>';
			
			$css .= '<link type = "text/css" href = "'._MODULE_DIR_.$this->name.'/css/zoomgeek.css" rel = "stylesheet"/>';
			$css .= '<link type = "text/css" href = "'._MODULE_DIR_.$this->name.'/css/fullsc.css" rel = "stylesheet"/>';			
    
			
			
			//get link and name produce
			//get picture
			$id_product = $_GET["id_product"];     
			$smarty->assign('id_pro',$id_product);

			$sql_img = "SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product = '$id_product'";
			$img_query = mysql_query($sql_img);
			$images = array();
			while($id_image = mysql_fetch_array($img_query)){
				$image = new Image($id_image['id_image']);
				$image_url = _THEME_PROD_DIR_.$image->getExistingImgPath();
				$images[] = $image_url;
			}
			//convert to string
			$url_img1 = http_build_query(array('z' => $images));
			$sPattern = '/&amp;+/';
			$sReplace = '%26';
			//replace "&amp;" by "%26" using in link
			$url_img =  preg_replace( $sPattern, $sReplace, $url_img1);

			//$url_img = urlencode($url_img1);
			$so = strlen($url_img);
			//$url_img = http_build_query(array('url_img' => $images));
			$smarty->assign('url_img',$url_img);
			$smarty->assign('so',$so);
			
			
			$sql_name = "SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_product = '$id_product' AND id_lang = 1";
			$name_query = mysql_query($sql_name);
			while($j = mysql_fetch_array($name_query)){
				$name_pro = urlencode($j["name"]);
			}
			
			$sql_name = "SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_product = '$id_product' AND id_lang = 1";
			$name_query = mysql_query($sql_name);
			while($j = mysql_fetch_array($name_query)){
				$name_pro = urlencode($j["name"]);
			}

			$smarty->assign('name_pro',$name_pro);
						
			$count_img = count($images);
			$smarty->assign('count_img',$count_img);
			
			
			//style zoom image using "jquery.anything.js"
		if(Configuration::get('style_zoom') == 1) {
			$css .='<link type = "text/css" href = "'._MODULE_DIR_.$this->name.'/css/anythingzoomer.css" rel = "stylesheet"/>';
			$js .='<script type="text/javascript" src="'._MODULE_DIR_.$this->name.'/js/jquery.anythingzoomer.js"></script>';
			$js .= '<script type = "text/javascript">
			$(document).ready(function(){
					var img_gz_o1 = $(\'#bigpic\').attr("src");
					var img_gz_length1= img_gz_o1.length - 10;
					var img_gz1 = img_gz_o1.substring(0,img_gz_length1) + "-thickbox.jpg";
					document.getElementById(\'image-block\').innerHTML = \'<div class="large"><img src="\'+img_gz1+\'" alt="big rushmore"></div><div class="small"><img src="\'+img_gz_o1+\'" id="bigpic" width="300" height="300" style="display: inline;" alt="small rushmore"></div>\';
					$(\'#image-block\').anythingZoomer();
			});
			</script>';		
		$js .= '
			<script type="text/javascript">
			$(document).ready(function(){
				$(\'#image-block\').hover(function(){
					var img_gz_o = $(\'#bigpic\').attr("src");
					var img_gz_length= img_gz_o.length - 10;
					var img_gz = img_gz_o.substring(0,img_gz_length) + "-thickbox.jpg";													
					$(\'.az-large-inner img\').attr("src",img_gz);											
				});
			});	
			</script>';
			}
	
			
			switch (Configuration::get('option_style')) {
					case "app":
							$js .= '
								<script  type = "text/javascript">
									$(document).ready(function() {
										document.getElementById("image-block").setAttribute("onClick", "goFullScreen_app();");
								});</script>';
							break;
							
					case "flash":
							$js .= '
								<script  type = "text/javascript">
									$(document).ready(function() {
										$(\'#image-block\').css("position","relative");
										$(\'#image-block\').append("<span id=\'gz_over\'><object width=\'34px\' height=\'35px\' id=\'demoSwf1\' data=\''._MODULE_DIR_.$this->name.'/superstage.swf\' style=\'visibility: visible;\' type=\'application/x-shockwave-flash\'><param value=\'false\' name=\'menu\'><param value=\'#FFFFFF\' name=\'bgcolor\'><param value=\'true\' name=\'allowfullscreen\'><param value=\'host='._MODULE_DIR_.$this->name.'%2Fxml%2F%3F'.$url_img.'%26name_pro='.$name_pro.'%26color='.Configuration::get('bg_color').'\' name=\'flashvars\'></object></span>");				
								});</script>';
							break;
							
					case "css3":
							$js .= '
								<script  type = "text/javascript">
									$(document).ready(function() {
										document.getElementById("image-block").setAttribute("onClick", "goFullScreen_css3();");
								});</script>';
							break;
			}
			
			
			//fulscreen image when click - hide default text "full_size" mặc định - add class - embeld flash
			$js .= '
			<script  type = "text/javascript">
				$(document).ready(function() {
					$(\'#view_full_size\').hide();
					$(\'#fancybox-tmp\').hide();							
			});</script>';
			
			
			$smarty->assign('js_zg',$js);
			$smarty->assign('css_zg',$css);
			
			return $this->display(__FILE__, 'Gpviewer-header.tpl');
		}
		
		//cach khac
		public function hookExtraRight ($params){
			global $smarty;
			$url_md = _MODULE_DIR_.$this->name;
			$smarty->assign('url_md',$url_md);			
			$smarty->assign('color',Configuration::get('bg_color'));
			
			//lấy ảnh
			$id_product = $_GET["id_product"];     
			$smarty->assign('id_pro',$id_product);

			$sql_img = "SELECT id_image FROM "._DB_PREFIX_."image WHERE id_product = '$id_product'";
			$img_query = mysql_query($sql_img);
			$images = array();
			while($id_image = mysql_fetch_array($img_query)){
				$image = new Image($id_image['id_image']);
				$image_url = _THEME_PROD_DIR_.$image->getExistingImgPath();
				$images[] = $image_url;
			}
			//convert to string
			$url_img1 = http_build_query(array('z' => $images));
			$sPattern = '/&amp;+/';
			$sReplace = '%26';
			$url_img =  preg_replace( $sPattern, $sReplace, $url_img1);
			
			$smarty->assign('img',$images);			
			$smarty->assign('url_img',$url_img);

			$sql_name = "SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_product = '$id_product' AND id_lang = 1";
			$name_query = mysql_query($sql_name);
			while($j = mysql_fetch_array($name_query)){
				$name_pro = urlencode($j["name"]);
			}			
			$sql_name = "SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_product = '$id_product' AND id_lang = 1";
			$name_query = mysql_query($sql_name);
			while($j = mysql_fetch_array($name_query)){
				$name_pro = urlencode($j["name"]);
			}

			$smarty->assign('name_pro',$name_pro);						
			$count_img = count($images);
			$smarty->assign('count_img',$count_img);
			
				
			return $this->display(__FILE__, 'Gpviewer.tpl');
		}
}	
?>

