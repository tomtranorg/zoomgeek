<?php
	
// http://coffeerings.posterous.com/php-simplexml-and-cdata

	header('Content-type: application/xml');
	$xml = new XmlWriter();
	$xml->openMemory();
	$xml->startDocument('1.0', 'UTF-8');
	
	$name_pro = $_GET["name_pro"];
	$url_img = $_GET["z"];
		
	$xml->startElement('superstage');//bắt đầu superstage	
		$xml->startElement('config'); //config		
			$xml->startElement('version');
			$xml->text($name_pro);
			$xml->endElement();
							
			$xml->startElement('stageColor');
			$xml->text("0xFFFFFF");
			$xml->endElement();
			
			$xml->startElement('showLabels');
			$xml->text("1");
			$xml->endElement();
			
			$xml->startElement('showIcons');
			$xml->text("1");
			$xml->endElement();
			
			$xml->startElement('enableSound');
			$xml->text("1");
			$xml->endElement();
			
		$xml->endElement(); //đóng config
		
		//bắt đầu product
		$xml->startElement('product');
		
			$xml->startElement('name');
			$xml->text($name_pro);
			$xml->endElement();
			
		$xml->endElement(); //đóng product
		
		//bắt đầu images
		$xml->startElement('images');
		
/*		$xml->startElement('image');
			
				// CData label
			  $xml->startElement('label');
			  $xml->writeCData('hiendaika');
			  $xml->endElement();
				
				// CData path
			  $xml->startElement('path');
			  $xml->writeCData('modules/zoomgeek/img/1.jpg');
			  $xml->endElement();
			
		  $xml->endElement();*/
	
		foreach( $url_img as $image ){
		  $xml->startElement('image');
			
				// CData label
			  $xml->startElement('label');
			  $xml->writeCData($name_pro);
			  $xml->endElement();
				
				// CData path
			  $xml->startElement('path');
				$xml->writeCData($image.'.jpg');
			  //$xml->writeCData('img/p/3/7/'.$image.'-large.jpg');
			  $xml->endElement();
			
		  $xml->endElement();
		}
		
		
		// end the document and output
		$xml->endElement(); //đóng images
	
	$xml->endElement();//kết thúc superstage
	echo $xml->outputMemory(true);

?>
