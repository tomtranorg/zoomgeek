<?php
/*
* Module name: Gpviewer
* by GeekPolis.com 2011
*
* This slideshow module is allows easy set up of dynamic
* display of product's images
*
* @author prestashopgeek.com <webmaster@prestashopgeek.com>
* @copyright 2012 prestashopgeek.com
* @file xml/index.php
* @version Release: 1.4.x - 2.2
*/
	header('Content-type: application/xml');
	$xml = new XmlWriter();
	$xml->openMemory();
	$xml->startDocument('1.0', 'UTF-8');
	
	$url_img = $_GET["z"];
	$name_pro = $_GET["name_pro"];
	$label = 1;
	$icon = 1;
	$sound = 1;
	$color = $_GET["color"];
	
		
	$xml->startElement('superstage');//bắt đầu superstage	
		$xml->startElement('config'); //config		
			$xml->startElement('version');
			$xml->text($name_pro);
			$xml->endElement();
							
			$xml->startElement('stageColor');
			$xml->text('0x'.$color);
			$xml->endElement();
			
			$xml->startElement('showLabels');
			$xml->text($label);
			$xml->endElement();
			
			$xml->startElement('showIcons');
			$xml->text('1');
			$xml->endElement();
			
			$xml->startElement('enableSound');
			$xml->text($sound);
			$xml->endElement();
			
		$xml->endElement(); //đóng config
		
		//bắt đầu product
		$xml->startElement('product');
		
			$xml->startElement('name');
			$xml->text($name_pro);
			$xml->endElement();
			
		$xml->endElement(); //đóng product
		
		//bắt đầu images
		$xml->startElement('images');
	
		foreach( $url_img as $image ){
		  $xml->startElement('image');
			
				// CData label
			  $xml->startElement('label');
			  $xml->writeCData('');
			  $xml->endElement();
				
				// CData path
			  $xml->startElement('path');
				$xml->writeCData($image.'-thickbox.jpg');
			  $xml->endElement();
			
		  $xml->endElement();
		}
		
		
		// end the document and output
		$xml->endElement(); //đóng images
	
	$xml->endElement();//kết thúc superstage
	echo $xml->outputMemory(true);

?>
